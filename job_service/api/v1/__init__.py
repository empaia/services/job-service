from .apps import add_routes_apps
from .jobs import add_routes_jobs


def add_routes_v1(app, late_init):
    add_routes_apps(app, late_init)
    add_routes_jobs(app, late_init)
