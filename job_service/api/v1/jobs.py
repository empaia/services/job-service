from fastapi import Body, Path, Query
from fastapi.exceptions import HTTPException
from pydantic import UUID4
from typing_extensions import Annotated

from ...models.v1.commons import IdObject, ItemCount
from ...models.v1.job import Job, JobList, JobQuery, JobStatus, JobToken, PostJob, PutJobStatus
from .commons import (
    DEPRECATED_STATUSES,
    END_STATUSES,
    END_STATUSES_WITH_MSG,
    JOB_STATUS_ORDER,
    JOB_STATUS_PREDECESSORS,
    get_ead,
    get_job,
)
from .db import db_clients
from .singletons import access_token_tools


def add_routes_jobs(app, late_init):
    @app.post(
        "/jobs",
        response_model=Job,
        responses={
            200: {"description": "The full newly created Job"},
            400: {"description": "App/EAD not found"},
        },
    )
    async def _(
        post_job: PostJob = Body(..., description="Core attributes of the Job to be created"),
    ) -> Job:
        """Checks job request, e.g. whether the app exists, derives full Job object and stores it in database."""
        # test whether App is known to Job Service, raises exception if not
        async with late_init.pool.acquire() as conn:
            app_client, job_client = await db_clients(conn=conn)

            await get_ead(app_client=app_client, app_id=post_job.app_id)

            return await job_client.add(post_job)

    @app.get(
        "/jobs/{job_id}/token",
        response_model=JobToken,
        responses={
            200: {"description": "The newly created Access Token"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to create the Token for"),
        expire: Annotated[int, Query(gt=0, description="Valid time in seconds (default: valid for 24h)")] = 86400,
    ) -> JobToken:
        """Create and return an Access-Token that is needed by the Job-Execution-Service to run the Job."""
        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)
            job = await get_job(job_client=job_client, job_id=job_id)
        # derive access token (requires Job ID)
        # note: right now, the Access Token is ONLY returned to the creator, but NOT stored in the Job Service
        # (it is stored in the Job Execution Service, though, as it is needed for delayed start of queued Jobs)
        token = access_token_tools.create_token(subject=str(job.id), expires_after_seconds=expire)
        return JobToken(job_id=job.id, access_token=token)

    @app.get(
        "/jobs/{job_id}",
        response_model=Job,
        responses={
            200: {"description": "The corresponding Job, with or without embedded EAD"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to retrieve"),
        with_ead: bool = Query(False, description="Whether to include the full EAD"),
    ) -> Job:
        """Get Job from database and return it, with all details and current status,
        or raise exception if it does not exist. Optionally include full EAD.
        """
        async with late_init.pool.acquire() as conn:
            app_client, job_client = await db_clients(conn=conn)

            job = await get_job(job_client=job_client, job_id=job_id)
            if with_ead:
                job.ead = await get_ead(app_client=app_client, app_id=job.app_id)
        return job

    @app.delete(
        "/jobs/{job_id}",
        response_model=Job,
        responses={
            200: {"description": "The removed Job"},
            400: {"description": "Job not found or invalid state"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to remove"),
    ) -> Job:
        """Delete Job from database, if it exists and still in state ASSEMBLY, otherwise raise error.
        The deleted Job is returned by the service.
        """
        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)

            deleted_job = await job_client.delete(job_id)

            if deleted_job is None:
                job = await get_job(job_client=job_client, job_id=job_id)
                raise HTTPException(
                    status_code=400,
                    detail={
                        "cause": f"Job has wrong state: {job.status}; can only delete in state {JobStatus.ASSEMBLY}"
                    },
                )

            return deleted_job

    @app.get(
        "/jobs",
        response_model=JobList,
        responses={
            200: {"description": "List of Jobs (or sublist, with paging)"},
        },
    )
    async def _(
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
    ) -> JobList:
        """
        Get list of all Jobs, optionally with skip and limit parameters for paging.
        Jobs are returned sorted by created_at field, descending order, newest first.
        """
        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)

            return await job_client.get_all(skip=skip, limit=limit)

    @app.put(
        "/jobs/query",
        response_model=JobList,
        responses={
            200: {"description": "List of matching items, including total number available"},
        },
    )
    async def _(
        query: JobQuery = Body(..., description="Query for filtering the returned items"),
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
    ) -> JobList:
        """Get list of filtered Jobs, with paging."""
        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)

            return await job_client.get_all(query=query, skip=skip, limit=limit)

    @app.put(
        "/jobs/{job_id}/status",
        responses={
            200: {"description": "Job object after Status update"},
            400: {"description": "Job not found or invalid status transition"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job whose status to update"),
        job_status: PutJobStatus = Body(..., description="New status and optional error message"),
    ) -> Job:
        """Get job from DB and update status; may be called by the app-service (for app-triggered completion) or WBS.
        Raise Exception if job not found or status invalid or invalid status transition.
        """
        if job_status.error_message is not None and job_status.status not in END_STATUSES_WITH_MSG:
            error_msg = f"Can only set status message for states {END_STATUSES_WITH_MSG}"
            raise HTTPException(status_code=400, detail={"cause": error_msg})

        if job_status.status in DEPRECATED_STATUSES:
            error_msg = f"Terminal Statuses {DEPRECATED_STATUSES} have been deprecated."
            raise HTTPException(status_code=400, detail={"cause": error_msg})

        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)

            job = await get_job(job_client=job_client, job_id=job_id)

            # set timestamps accordingly
            started_at = job_status.status == JobStatus.RUNNING
            ended_at = job_status.status in END_STATUSES

            if JOB_STATUS_ORDER[job_status.status] == 0:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Illegal status transition from {job.status} to {job_status.status}"},
                )

            where_statuses = JOB_STATUS_PREDECESSORS[job_status.status]
            updated_job = await job_client.update(
                job_id=job_id,
                job_status=job_status,
                started_at=started_at,
                ended_at=ended_at,
                where_statuses=where_statuses,
            )

            if updated_job is None:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Illegal status transition from {job.status} to {job_status.status}"},
                )

            return updated_job

    @app.put(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        responses={
            200: {"description": "Job object with added input"},
            400: {"description": "Unknown input or input already set"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to add an input to"),
        input_key: str = Path(..., description="The name of the input to set"),
        input_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the input"),
    ) -> Job:
        """The job is created with no inputs. Use this to add inputs to the Job after creation, but before
        executing the Job. Set Job Status to `READY` when all inputs have been set (not done automatically).
        Raises Exception if job not found.
        """
        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)

            job = await get_job(job_client=job_client, job_id=job_id)
            if job.status != JobStatus.ASSEMBLY:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Can only add Inputs in state {JobStatus.ASSEMBLY}, but found {job.status}"},
                )

            if input_key in job.inputs:
                raise HTTPException(
                    status_code=400, detail={"cause": f"Key already has a value in Job's inputs: {input_key}"}
                )

            updated_job = await job_client.set_input(job_id=job_id, input_key=input_key, input_id=input_id.id)
            if updated_job is None:
                raise HTTPException(
                    status_code=400, detail={"cause": "Could not update Job's inputs: likely a race condition"}
                )
            return updated_job

    @app.delete(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        responses={
            200: {"description": "Job object with deleted input"},
            400: {"description": "Unknown input_key"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to delete an input from"),
        input_key: str = Path(..., description="The name of the input to delete"),
    ) -> Job:
        """Use this to delete already set inputs from the Job. Only works, if Job Status is `ASSEMBLY`.
        Raises Exception if job not found or input key does not exist or Status is not `ASSEMBLY`.
        """
        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)

            job = await get_job(job_client=job_client, job_id=job_id)
            if job.status != JobStatus.ASSEMBLY:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Can only delete Inputs in state {JobStatus.ASSEMBLY}, but found {job.status}"},
                )

            if input_key not in job.inputs:
                raise HTTPException(status_code=400, detail={"cause": f"Key not in Job's inputs: {input_key}"})

            updated_job = await job_client.delete_input(job_id=job_id, input_key=input_key)
            if updated_job is None:
                raise HTTPException(
                    status_code=400, detail={"cause": "Could not update Job's inputs: likely a race condition"}
                )
            return updated_job

    @app.put(
        "/jobs/{job_id}/outputs/{output_key}",
        response_model=Job,
        responses={
            200: {"description": "Job object with added output"},
            400: {"description": "Unknown output or output already set"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to add an output to"),
        output_key: str = Path(..., description="The name of the output to set"),
        output_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the output"),
    ) -> Job:
        """To be called by app-service to add output-references to the Job in the database.
        Raises Exception if job not found or output key does not match.
        """
        async with late_init.pool.acquire() as conn:
            _, job_client = await db_clients(conn=conn)

            job = await get_job(job_client=job_client, job_id=job_id)
            if job.status not in [JobStatus.READY, JobStatus.SCHEDULED, JobStatus.RUNNING]:
                raise HTTPException(
                    status_code=400,
                    detail={
                        "cause": f"Outputs allowed from {JobStatus.READY} - {JobStatus.RUNNING}, job in {job.status}"
                    },
                )

            if output_key in job.outputs:
                raise HTTPException(
                    status_code=400, detail={"cause": f"Key already has a value in Job's outputs: {output_key}"}
                )

            updated_job = await job_client.set_output(job_id=job_id, output_key=output_key, output_id=output_id.id)
            if updated_job is None:
                raise HTTPException(
                    status_code=400, detail={"cause": "Could not update Job's output: likely a race condition"}
                )
            return updated_job

    @app.get(
        "/public-key",
        response_model=bytes,
        responses={
            200: {"description": "Public key for validating Access Tokens created for Jobs"},
        },
    )
    async def _() -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Jobs."""
        return access_token_tools.public_key
