from asyncpg.exceptions import DataError
from fastapi.exceptions import HTTPException

from ...models.v1.job import JobStatus

# show which status can follow on which other status, e.g. RUNNING can follow on SCHEDULED, but not the other way around
JOB_STATUS_ORDER = {
    JobStatus.NONE: 0,
    JobStatus.ASSEMBLY: 1,
    JobStatus.READY: 2,
    JobStatus.SCHEDULED: 3,
    JobStatus.RUNNING: 4,
    JobStatus.COMPLETED: 5,
    JobStatus.FAILED: 5,
    JobStatus.ERROR: 5,
    JobStatus.TIMEOUT: 5,
    JobStatus.INCOMPLETE: 5,
}

END_STATUS_INDEX = 5
END_STATUSES = [key for key, val in JOB_STATUS_ORDER.items() if val == END_STATUS_INDEX]
END_STATUSES_WITH_MSG = [JobStatus.FAILED, JobStatus.ERROR, JobStatus.TIMEOUT]
DEPRECATED_STATUSES = [JobStatus.FAILED, JobStatus.TIMEOUT, JobStatus.INCOMPLETE]

JOB_STATUS_BY_INDEX = {}
for status, index in JOB_STATUS_ORDER.items():
    JOB_STATUS_BY_INDEX[index] = JOB_STATUS_BY_INDEX.get(index, [])
    JOB_STATUS_BY_INDEX[index].append(status)

JOB_STATUS_PREDECESSORS = {}
for status, index in JOB_STATUS_ORDER.items():
    if index == 0:
        continue
    JOB_STATUS_PREDECESSORS[status] = JOB_STATUS_PREDECESSORS.get(status, [])
    for i in range(index):
        JOB_STATUS_PREDECESSORS[status] += JOB_STATUS_BY_INDEX[i]


async def get_ead(app_client, app_id):
    try:
        ead = await app_client.get(app_id)
        if ead is None:
            raise HTTPException(status_code=400, detail={"cause": f"App not found: {app_id}  does not exist"})
        return ead
    except DataError as e:
        raise HTTPException(status_code=400, detail={"cause": "Could not get App: badly formed"}) from e


async def get_job(job_client, job_id):
    try:
        job = await job_client.get(job_id)
        if job is None:
            raise HTTPException(status_code=400, detail={"cause": f"Job not found: {job_id} does not exist"})
        return job
    except DataError as e:
        raise HTTPException(status_code=400, detail={"cause": "Job not found: badly formed"}) from e
