from typing import List, Optional

from asyncpg.exceptions import DataError
from fastapi.exceptions import HTTPException

from ....models.v1.job import Job, JobList, JobQuery, JobStatus, PostJob, PutJobStatus
from ....singletons import logger


class JobClient:
    """
    Client for simple Job data base
    """

    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _get_runtime_sql():
        return """
        CASE
            WHEN started_at IS NULL AND ended_at IS NULL
                THEN NULL
            WHEN status = 'RUNNING'
                THEN EXTRACT(EPOCH FROM CURRENT_TIMESTAMP)::int - EXTRACT(EPOCH FROM started_at)::int
            WHEN
                status = 'COMPLETED' OR
                status = 'FAILED' OR
                status = 'ERROR' OR
                status = 'TIMEOUT' OR
                status = 'INCOMPLETE'
                THEN EXTRACT(EPOCH FROM ended_at)::int - EXTRACT(EPOCH FROM started_at)::int
            ELSE NULL
        END
        """

    @staticmethod
    def _sql_job_as_json():
        return f"""json_build_object(
            'id', id,
            'app_id', app_id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'inputs', inputs,
            'outputs', outputs,
            'status', status,
            'error_message', error_message,
            'created_at', EXTRACT(EPOCH FROM created_at)::int,
            'started_at', EXTRACT(EPOCH FROM started_at)::int,
            'ended_at', EXTRACT(EPOCH FROM ended_at)::int,
            'runtime', {JobClient._get_runtime_sql()}
        )"""

    async def add(self, post_job: PostJob) -> Job:
        sql = f"""
        INSERT INTO jobs (app_id, creator_id, creator_type, inputs, outputs, status, created_at)
        VALUES ($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)
        async with self.conn.transaction():
            row = await self.conn.fetchrow(
                sql, post_job.app_id, post_job.creator_id, post_job.creator_type, {}, {}, JobStatus.ASSEMBLY
            )
            return Job(**row["json_build_object"])

    async def get(self, job_id: str) -> Optional[Job]:
        # (id, app_id, creator_id, creator_type, inputs, outputs, status, created_at, started_at, ended_at)
        sql = f"""
        SELECT {JobClient._sql_job_as_json()}
        FROM jobs
        WHERE id=$1;
        """
        logger.debug(sql)
        row = await self.conn.fetchrow(sql, job_id)
        if row is None:
            return
        return Job(**row["json_build_object"])

    @staticmethod
    def _query_transform(query: JobQuery, start_index=1):
        transformed_query = ""
        query_terms = []
        query_data = []

        if query is None:
            return transformed_query, query_data

        query_mapping = {
            "jobs.id": query.jobs,
            "jobs.creator_id": query.creators,
            "jobs.app_id": query.apps,
            "jobs.status": query.statuses,
        }

        i = start_index
        for key, val in query_mapping.items():
            if val is None:
                continue

            query_terms.append(f"{key}=ANY(${i})")
            query_data.append(val)
            i += 1

        transformed_query = JobClient._sql_concat(query_terms=query_terms, prefix="WHERE ", sep=" AND ")
        return transformed_query, query_data

    @staticmethod
    def _sql_concat(query_terms, prefix, sep):
        if len(query_terms) == 0:
            return ""
        return prefix + sep.join(query_terms)

    async def get_all(self, query: JobQuery = None, skip=None, limit=None) -> JobList:
        transformed_query, query_data = self._query_transform(query)

        sql = f"""
        SELECT json_agg({JobClient._sql_job_as_json()} ORDER BY sub.created_at DESC)
        FROM (
            SELECT *
            FROM jobs
            {transformed_query}
            ORDER BY created_at DESC
        """

        if limit is not None:
            sql += f"""    LIMIT {limit}
            """

        if skip is not None:
            sql += f"""    OFFSET {skip}
            """

        sql += """) AS sub;
        """
        logger.debug(sql)

        items = []
        if limit is None or limit > 0:
            row = await self.conn.fetchrow(sql, *query_data)
            if row["json_agg"] is not None:
                items = row["json_agg"]

        sql = f"""
        SELECT COUNT(id)
        FROM jobs
        {transformed_query};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, *query_data)
        item_count = row["count"]

        return JobList(items=items, item_count=item_count)

    async def update(
        self,
        job_id: str,
        job_status: PutJobStatus,
        started_at=False,
        ended_at=False,
        where_statuses: List[JobStatus] = None,
    ) -> Optional[Job]:
        data = [job_status.status.value]
        data_index = 3

        sql = """
        UPDATE jobs
        SET status=$2"""

        if job_status.error_message is not None:
            sql += f""",
            error_message=${data_index}"""
            data.append(job_status.error_message)
            data_index += 1

        if started_at:
            sql += """,
            started_at=CURRENT_TIMESTAMP"""

        if ended_at:
            sql += """,
            ended_at=CURRENT_TIMESTAMP"""

        sql += """
        WHERE id=$1"""

        if where_statuses is not None:
            sql += f""" AND status=ANY(${data_index})"""
            data.append([s.value for s in where_statuses])
            data_index += 1

        sql += f"""
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, job_id, *data)

        if row is None:
            return

        return Job.model_validate(row["json_build_object"])

    async def delete(self, job_id: str) -> Optional[Job]:
        sql = f"""
        DELETE FROM jobs
        WHERE id=$1 AND status='ASSEMBLY'
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)
        try:
            row = await self.conn.fetchrow(sql, job_id)
        except DataError as e:
            raise HTTPException(status_code=400, detail={"cause": "Job not found: badly formed"}) from e

        if row is None:
            return

        return Job.model_validate(row["json_build_object"])

    async def set_input(self, job_id, input_key: str, input_id: str) -> Optional[Job]:
        sql = f"""
        UPDATE jobs
        SET inputs = jsonb_set(inputs, $2, $3, true)
        WHERE id=$1 AND status='ASSEMBLY' AND NOT (inputs ?| $2)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, job_id, [input_key], input_id)

        if row is None:
            return

        return Job.model_validate(row["json_build_object"])

    async def delete_input(self, job_id, input_key: str) -> Optional[Job]:
        sql = f"""
        UPDATE jobs
        SET inputs = inputs - $2
        WHERE id=$1 AND status='ASSEMBLY' AND (inputs ?| $2)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, job_id, [input_key])

        if row is None:
            return

        return Job.model_validate(row["json_build_object"])

    async def set_output(self, job_id, output_key: str, output_id: str) -> Optional[Job]:
        sql = f"""
        UPDATE jobs
        SET outputs = jsonb_set(outputs, $2, $3, true)
        WHERE id=$1 AND (status='READY' OR status='SCHEDULED' OR status='RUNNING') AND NOT (outputs ?| $2)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, job_id, [output_key], output_id)

        if row is None:
            return

        return Job.model_validate(row["json_build_object"])
