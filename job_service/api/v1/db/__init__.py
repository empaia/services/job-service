from asyncpg import Connection

from ....db import set_type_codecs
from .app_client import AppClient
from .job_client import JobClient


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return AppClient(conn=conn), JobClient(conn=conn)
