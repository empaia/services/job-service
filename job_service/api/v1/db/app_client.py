from typing import Optional

from asyncpg import Connection


class AppClient:
    """Client for storing known Apps/EADs. Right now, EADs are stored as plain dicts, using the app_id as key"""

    def __init__(self, conn: Connection):
        self.conn = conn

    async def add(self, app_id: str, ead: dict):
        async with self.conn.transaction():
            sql = """
            INSERT INTO apps (id, ead)
            VALUES ($1, $2);
            """
            await self.conn.execute(sql, app_id, ead)

    async def get(self, app_id: str) -> Optional[dict]:
        sql = """
        SELECT ead FROM apps WHERE id=$1;
        """
        row = await self.conn.fetchrow(sql, app_id)
        if row is None:
            return
        return row["ead"]
