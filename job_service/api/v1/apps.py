from asyncpg.exceptions import DataError, UniqueViolationError
from fastapi import Body, Path
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ...ead_validation.py_ead_validation.ead_validator import validate_ead
from ...ead_validation.py_ead_validation.exceptions import EadValidationError
from ...singletons import logger
from .commons import get_ead
from .db import db_clients


def add_routes_apps(app, late_init):
    @app.put(
        "/apps/{app_id}/ead",
        responses={
            200: {"description": "EAD successfully stored"},
            400: {"description": "App already exists or can not be stored due to other error"},
        },
    )
    async def _(
        app_id: UUID4 = Path(..., description="The Id of the App corresponding to the EAD"),
        ead: dict = Body(..., description="The full EAD in accordance with the JSON Schema"),
    ):
        """Store EAD in database so it can be used in Jobs later."""
        async with late_init.pool.acquire() as conn:
            app_client, _ = await db_clients(conn=conn)

            try:
                validate_ead(ead, enable_legacy_support=True)
                await app_client.add(app_id, ead)
            except EadValidationError as e:
                raise HTTPException(status_code=400, detail={"cause": f"EAD Validation failed: {e}"}) from e
            except DataError as e:
                logger.debug(e)
                raise HTTPException(status_code=400, detail={"cause": "Could not store App: badly formed"}) from e
            except UniqueViolationError as e:
                logger.debug(e)
                # should be 409, but keeping 400 for backwards compatibility
                raise HTTPException(status_code=400, detail={"cause": f"App already exists: {app_id}"}) from e

    @app.get(
        "/apps/{app_id}/ead",
        responses={
            200: {"description": "JSON dictionary representing the EAD corresponding to the App Id"},
            400: {"description": "EAD not found or other problem when retrieving the EAD"},
        },
    )
    async def _(
        app_id: UUID4 = Path(..., description="The Id of the App corresponding to the EAD"),
    ) -> dict:
        """Get EAD for given App-ID from database."""
        async with late_init.pool.acquire() as conn:
            app_client, _ = await db_clients(conn=conn)

            return await get_ead(app_client=app_client, app_id=app_id)
