from asyncpg import Connection

from ....db import set_type_codecs
from .job_client import JobClient


async def db_client(conn: Connection):
    await set_type_codecs(conn=conn)
    return JobClient(conn=conn)
