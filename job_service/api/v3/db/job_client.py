from typing import List, Optional

from asyncpg.exceptions import DataError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException

from ....models.v3.job import (
    Job,
    JobList,
    JobQuery,
    JobStatus,
    JobValidationStatus,
    PostJob,
    PutJobProgress,
    PutJobStatus,
    PutJobValidationStatus,
    SelectorValue,
)
from ....singletons import logger


class JobClient:
    """
    Client for simple Job data base
    """

    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _get_runtime_sql():
        return """
        CASE
            WHEN started_at IS NULL AND ended_at IS NULL
                THEN NULL
            WHEN status = 'RUNNING'
                THEN EXTRACT(EPOCH FROM CURRENT_TIMESTAMP)::int - EXTRACT(EPOCH FROM started_at)::int
            WHEN
                status = 'COMPLETED' OR
                status = 'FAILED' OR
                status = 'ERROR' OR
                status = 'TIMEOUT' OR
                status = 'INCOMPLETE'
                THEN EXTRACT(EPOCH FROM ended_at)::int - EXTRACT(EPOCH FROM started_at)::int
            ELSE NULL
        END
        """

    @staticmethod
    def _sql_job_as_json():
        return f"""json_build_object(
            'id', id,
            'app_id', app_id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'mode', mode,
            'containerized', containerized,
            'inputs', inputs,
            'outputs', outputs,
            'status', status,
            'error_message', error_message,
            'created_at', EXTRACT(EPOCH FROM created_at)::int,
            'started_at', EXTRACT(EPOCH FROM started_at)::int,
            'progress', progress,
            'ended_at', EXTRACT(EPOCH FROM ended_at)::int,
            'runtime', {JobClient._get_runtime_sql()},
            'input_validation_status', input_validation_status,
            'input_validation_error_message', input_validation_error_message,
            'output_validation_status', output_validation_status,
            'output_validation_error_message', output_validation_error_message
        )"""

    async def add(self, post_job: PostJob) -> Job:
        sql = f"""
        INSERT INTO v3_jobs (app_id, creator_id, creator_type, mode, containerized, inputs, outputs,
                             status, input_validation_status, output_validation_status, created_at)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, CURRENT_TIMESTAMP)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)
        try:
            async with self.conn.transaction():
                row = await self.conn.fetchrow(
                    sql,
                    post_job.app_id,
                    post_job.creator_id,
                    post_job.creator_type,
                    post_job.mode,
                    post_job.containerized,
                    {},
                    {},
                    JobStatus.ASSEMBLY,
                    JobValidationStatus.NONE,
                    JobValidationStatus.NONE,
                )
                return Job(**row["json_build_object"])
        except UniqueViolationError as e:
            raise HTTPException(409, "There already exists a report job for the creator_id") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to create Job in database") from e

    @staticmethod
    def _sql_selector_value_as_json():
        return """json_build_object(
            'id', id,
            'input_key', input_key,
            'selector_value', selector_value
        )"""

    async def update_selector_value(self, job_id, input_key: str, selector_value: str) -> Optional[SelectorValue]:
        sql = f"""
        INSERT INTO v3_selector_values (id, input_key, selector_value)
        VALUES ($1, $2, $3)
        ON CONFLICT (id, input_key) DO UPDATE
        SET selector_value=$3 WHERE v3_selector_values.id=$1 AND v3_selector_values.input_key=$2
        RETURNING {JobClient._sql_selector_value_as_json()};
        """
        logger.debug(sql)
        try:
            row = await self.conn.fetchrow(sql, job_id, input_key, selector_value)
            return row if row is None else SelectorValue(**row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to add input_key and selector_value in database") from e

    async def get_selector_value(self, job_id, input_key: str) -> Optional[SelectorValue]:
        sql = f"""
        SELECT {JobClient._sql_selector_value_as_json()}
        FROM v3_selector_values
        WHERE id=$1 AND input_key=$2;
        """
        logger.debug(sql)
        try:
            row = await self.conn.fetchrow(sql, job_id, input_key)
            return row if row is None else SelectorValue(**row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to retrieve selector_value from database") from e

    async def get(self, job_id: str) -> Optional[Job]:
        # (id, app_id, creator_id, creator_type, inputs, outputs, status, created_at, started_at, ended_at)
        sql = f"""
        SELECT {JobClient._sql_job_as_json()}
        FROM v3_jobs
        WHERE id=$1;
        """
        logger.debug(sql)
        row = await self.conn.fetchrow(sql, job_id)
        if row is None:
            return
        return Job(**row["json_build_object"])

    @staticmethod
    def _query_transform(query: JobQuery, start_index=1):
        transformed_query = ""
        query_terms = []
        query_data = []

        if query is None:
            return transformed_query, query_data

        query_mapping = {
            "v3_jobs.id": query.jobs,
            "v3_jobs.creator_id": query.creators,
            "v3_jobs.app_id": query.apps,
            "v3_jobs.status": query.statuses,
            "v3_jobs.mode": query.modes,
            "v3_jobs.creator_type": query.creator_types,
            "v3_jobs.input_validation_status": query.input_validation_statuses,
            "v3_jobs.output_validation_status": query.output_validation_statuses,
        }

        i = start_index
        for key, val in query_mapping.items():
            if val is None:
                continue

            query_terms.append(f"{key}=ANY(${i})")
            query_data.append(val)
            i += 1

        transformed_query = JobClient._sql_concat(query_terms=query_terms, prefix="WHERE ", sep=" AND ")
        return transformed_query, query_data

    @staticmethod
    def _sql_concat(query_terms, prefix, sep):
        if len(query_terms) == 0:
            return ""
        return prefix + sep.join(query_terms)

    async def get_all(self, query: JobQuery = None, skip=None, limit=None) -> JobList:
        transformed_query, query_data = self._query_transform(query)

        sql = f"""
        SELECT json_agg({JobClient._sql_job_as_json()} ORDER BY sub.created_at DESC)
        FROM (
            SELECT *
            FROM v3_jobs
            {transformed_query}
            ORDER BY created_at DESC
        """

        if limit is not None:
            sql += f"""    LIMIT {limit}
            """

        if skip is not None:
            sql += f"""    OFFSET {skip}
            """

        sql += """) AS sub;
        """
        logger.debug(sql)

        items = []
        if limit is None or limit > 0:
            row = await self.conn.fetchrow(sql, *query_data)
            if row["json_agg"] is not None:
                items = row["json_agg"]

        sql = f"""
        SELECT COUNT(id)
        FROM v3_jobs
        {transformed_query};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, *query_data)
        item_count = row["count"]

        return JobList(items=items, item_count=item_count)

    async def update_progress(self, job_id: str, job_progress: PutJobProgress):
        sql = f"""
        UPDATE v3_jobs
        SET progress=$2 WHERE id=$1
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id, job_progress.progress)
            return row if row is None else Job.model_validate(row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to update Job progress in database") from e

    async def update(
        self,
        job_id: str,
        job_status: PutJobStatus,
        started_at=False,
        ended_at=False,
        where_statuses: List[JobStatus] = None,
    ) -> Optional[Job]:
        data = [job_status.status.value]
        data_index = 3

        sql = """
        UPDATE v3_jobs
        SET status=$2"""

        if job_status.error_message is not None:
            sql += f""",
            error_message=${data_index}"""
            data.append(job_status.error_message)
            data_index += 1

        if started_at:
            sql += """,
            started_at=CURRENT_TIMESTAMP"""

        if ended_at:
            sql += """,
            ended_at=CURRENT_TIMESTAMP"""

        sql += """
        WHERE id=$1"""

        if where_statuses is not None:
            sql += f""" AND status=ANY(${data_index})"""
            data.append([s.value for s in where_statuses])
            data_index += 1

        sql += f"""
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id, *data)
            return row if row is None else Job.model_validate(row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to update Job in database") from e

    async def delete(self, job_id: str) -> Optional[Job]:
        sql = f"""
        DELETE FROM v3_jobs
        WHERE id=$1 AND status='ASSEMBLY'
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id)
            return row if row is None else Job.model_validate(row["json_build_object"])
        except DataError as e:
            raise HTTPException(status_code=400, detail={"cause": "Job not found: badly formed"}) from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to delete Job in database") from e

    async def set_input(self, job_id, input_key: str, input_id: str) -> Optional[Job]:
        sql = f"""
        UPDATE v3_jobs
        SET inputs = jsonb_set(inputs, $2, $3, true)
        WHERE id=$1 AND status='ASSEMBLY' AND NOT (inputs ?| $2)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id, [input_key], input_id)
            return row if row is None else Job.model_validate(row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to set input for Job in database") from e

    async def delete_input(self, job_id, input_key: str) -> Optional[Job]:
        sql = f"""
        UPDATE v3_jobs
        SET inputs = inputs - $2
        WHERE id=$1 AND status='ASSEMBLY' AND (inputs ?| $2)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id, [input_key])
            return row if row is None else Job.model_validate(row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to delete input for Job in database") from e

    async def delete_output(self, job_id, output_key: str) -> Optional[Job]:
        sql = f"""
        UPDATE v3_jobs
        SET outputs = outputs - $2
        WHERE id=$1 AND status='ASSEMBLY' AND (outputs ?| $2)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id, [output_key])
            return row if row is None else Job.model_validate(row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to delete output for Job in database") from e

    async def set_output(self, job_id, output_key: str, output_id: str) -> Optional[Job]:
        sql = f"""
        UPDATE v3_jobs
        SET outputs = jsonb_set(outputs, $2, $3, true)
        WHERE id=$1
            AND (
                ((mode='REPORT' OR (mode='POSTPROCESSING' and containerized=false)) AND status='ASSEMBLY')
                OR (
                    (mode in ('STANDALONE', 'PREPROCESSING') OR (mode='POSTPROCESSING' AND containerized=true))
                    AND status in ('READY', 'SCHEDULED', 'RUNNING')
                )
            )
            AND NOT (outputs ?| $2)
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id, [output_key], output_id)
            return row if row is None else Job.model_validate(row["json_build_object"])
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to set output for Job in database") from e

    async def update_input_validation_status(self, job_id, status_update: PutJobValidationStatus):
        return await self._update_validation_status("input", job_id, status_update)

    async def update_output_validation_status(self, job_id, status_update: PutJobValidationStatus):
        return await self._update_validation_status("output", job_id, status_update)

    async def _update_validation_status(self, i_or_o, job_id, status_update: PutJobValidationStatus):
        sql = f"""
        UPDATE v3_jobs
        SET {i_or_o}_validation_status = $2, {i_or_o}_validation_error_message = $3
        WHERE id=$1
        RETURNING {JobClient._sql_job_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, job_id, status_update.validation_status, status_update.error_message)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Failed to update input validation status") from e
        if not row:
            raise HTTPException(400, "Failed to update input validation status")
        return Job.model_validate(row["json_build_object"])
