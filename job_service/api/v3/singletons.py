from ...models.utils.access_token_tools import AccessTokenTools
from ...singletons import rsa_keys_directory

access_token_tools = AccessTokenTools(f"{rsa_keys_directory}/v3")
