from fastapi import Body, Path, Query
from fastapi.exceptions import HTTPException
from pydantic import UUID4
from typing_extensions import Annotated

from ...models.v3.commons import IdObject, ItemCount
from ...models.v3.job import (
    Job,
    JobList,
    JobMode,
    JobQuery,
    JobStatus,
    JobToken,
    JobValidationStatus,
    PostJob,
    PutJobProgress,
    PutJobStatus,
    PutJobValidationStatus,
    PutSelectorValue,
    SelectorValue,
)
from ...singletons import settings
from .commons import (
    DEPRECATED_STATUSES,
    END_STATUSES,
    END_STATUSES_WITH_MSG,
    JOB_STATUS_ORDER,
    JOB_STATUS_PREDECESSORS,
    aggregate_job,
    get_job,
)
from .db import db_client
from .singletons import access_token_tools


def add_routes_jobs(app, late_init):
    @app.post(
        "/jobs",
        response_model=Job,
        responses={
            200: {"description": "The full newly created Job"},
        },
    )
    async def _(
        post_job: PostJob = Body(..., description="Core attributes of the Job to be created"),
    ) -> Job:
        """Checks job request, e.g. whether the app exists, derives full Job object and stores it in database."""
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            return await job_client.add(post_job)

    @app.get(
        "/jobs/{job_id}/token",
        response_model=JobToken,
        responses={
            200: {"description": "The newly created Access Token"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to create the Token for"),
        expire: Annotated[int, Query(gt=0, description="Valid time in seconds (default: valid for 24h)")] = 86400,
    ) -> JobToken:
        """Create and return an Access-Token that is needed by the Job-Execution-Service to run the Job."""
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            job = await get_job(job_client=job_client, job_id=job_id)
        # derive access token (requires Job ID)
        # note: right now, the Access Token is ONLY returned to the creator, but NOT stored in the Job Service
        # (it is stored in the Job Execution Service, though, as it is needed for delayed start of queued Jobs)
        token = access_token_tools.create_token(subject=str(job.id), expires_after_seconds=expire)
        return JobToken(job_id=job.id, access_token=token)

    @app.get(
        "/jobs/{job_id}",
        response_model=Job,
        responses={
            200: {"description": "The corresponding Job"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to retrieve"),
    ) -> Job:
        """Get Job from database and return it, with all details and current status,
        or raise exception if it does not exist.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            return await get_job(job_client=job_client, job_id=job_id)

    @app.get(
        "/jobs/{job_id}/inputs/{input_key}/selector",
        response_model=SelectorValue,
        responses={
            200: {"description": "The Selector Value of the specified Job Id and input key"},
            404: {"description": "Job Id or input key not found"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Job Id of the Selector Value to retrieve"),
        input_key: str = Path(..., description="The input key of the Selector Value to retrieve"),
    ) -> SelectorValue:
        """Get Selector Value of Job with the corresponding Job Id and input key from DB and return it,
        or raise exception if either one of the parameters does not exist.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            job = await get_job(job_client=job_client, job_id=job_id)

            if input_key not in job.inputs:
                raise HTTPException(
                    status_code=412, detail={"cause": f"{input_key} not found in Job inputs with specified Job Id"}
                )

            selector_value = await job_client.get_selector_value(job_id=job_id, input_key=input_key)

            if selector_value is None:
                raise HTTPException(
                    status_code=404,
                    detail={"cause": "No selector_value found for specified job_id and input_key"},
                )
            return selector_value

    @app.delete(
        "/jobs/{job_id}",
        response_model=Job,
        responses={
            200: {"description": "The removed Job"},
            400: {"description": "Job not found or invalid state"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to remove"),
    ) -> Job:
        """Delete Job from database, if it exists and still in state ASSEMBLY, otherwise raise error.
        The deleted Job is returned by the service.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            deleted_job = await job_client.delete(job_id)

            if deleted_job is None:
                job = await get_job(job_client=job_client, job_id=job_id)
                raise HTTPException(
                    status_code=400,
                    detail={
                        "cause": f"Job has wrong state: {job.status}; can only delete in state {JobStatus.ASSEMBLY}"
                    },
                )

            return deleted_job

    @app.get(
        "/jobs",
        response_model=JobList,
        responses={
            200: {"description": "List of Jobs (or sublist, with paging)"},
        },
    )
    async def _(
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
    ) -> JobList:
        """
        Get list of all Jobs, optionally with skip and limit parameters for paging.
        Jobs are returned sorted by created_at field, descending order, newest first.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            return await job_client.get_all(skip=skip, limit=limit)

    @app.put(
        "/jobs/query",
        response_model=JobList,
        responses={
            200: {"description": "List of matching items, including total number available"},
        },
    )
    async def _(
        query: JobQuery = Body(..., description="Query for filtering the returned items"),
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
    ) -> JobList:
        """Get list of filtered Jobs, with paging."""
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            return await job_client.get_all(query=query, skip=skip, limit=limit)

    @app.put(
        "/jobs/{job_id}/status",
        responses={
            200: {"description": "Job object after Status update"},
            400: {"description": "Job not found or invalid status transition"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job whose status to update"),
        job_status: PutJobStatus = Body(..., description="New status and optional error message"),
    ) -> Job:
        """Get job from DB and update status; may be called by the app-service (for app-triggered completion) or WBS.
        Raise Exception if job not found or status invalid or invalid status transition.
        """
        if job_status.error_message is not None and job_status.status not in END_STATUSES_WITH_MSG:
            error_msg = f"Can only set status message for states {END_STATUSES_WITH_MSG}"
            raise HTTPException(status_code=400, detail={"cause": error_msg})

        if job_status.status in DEPRECATED_STATUSES:
            error_msg = f"Terminal Statuses {DEPRECATED_STATUSES} have been deprecated."
            raise HTTPException(status_code=400, detail={"cause": error_msg})

        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            job = await get_job(job_client=job_client, job_id=job_id)

            # set timestamps accordingly
            started_at = job_status.status == JobStatus.RUNNING
            ended_at = job_status.status in END_STATUSES

            await _validate_state_transitions(job_status, job)

            if settings.allow_any_state_transition:
                where_statuses = None
            else:
                where_statuses = JOB_STATUS_PREDECESSORS[job_status.status]

            if settings.annot_url:
                if job_status.status in [JobStatus.READY, JobStatus.COMPLETED]:
                    aggregated = await aggregate_job(job_id)
                    if aggregated:
                        updated_job = await job_client.update(
                            job_id=job_id,
                            job_status=job_status,
                            started_at=started_at,
                            ended_at=ended_at,
                            where_statuses=where_statuses,
                        )
                    else:
                        updated_job = await job_client.update(
                            job_id=job_id,
                            job_status=PutJobStatus(
                                status=JobStatus.ERROR, error_message="Error during job data aggregation!"
                            ),
                            started_at=started_at,
                            ended_at=True,
                            where_statuses=where_statuses,
                        )
                else:
                    updated_job = await job_client.update(
                        job_id=job_id,
                        job_status=job_status,
                        started_at=started_at,
                        ended_at=ended_at,
                        where_statuses=where_statuses,
                    )
            else:
                updated_job = await job_client.update(
                    job_id=job_id,
                    job_status=job_status,
                    started_at=started_at,
                    ended_at=ended_at,
                    where_statuses=where_statuses,
                )

            if updated_job is None:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Illegal status transition from {job.status} to {job_status.status}"},
                )

            return updated_job

    @app.put(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        responses={
            200: {"description": "Job object with added input"},
            400: {"description": "Unknown input or input already set"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to add an input to"),
        input_key: str = Path(..., description="The name of the input to set"),
        input_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the input"),
    ) -> Job:
        """The job is created with no inputs. Use this to add inputs to the Job after creation, but before
        executing the Job. Set Job Status to `READY` when all inputs have been set (not done automatically).
        Raises Exception if Job not found.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            job = await get_job(job_client=job_client, job_id=job_id)
            if job.status != JobStatus.ASSEMBLY:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Can only add Inputs in state {JobStatus.ASSEMBLY}, but found {job.status}"},
                )

            if job.mode in [JobMode.REPORT]:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Adding Inputs for job mode {job.mode} is not allowed"},
                )

            if input_key in job.inputs:
                raise HTTPException(
                    status_code=400, detail={"cause": f"Key already has a value in Job's inputs: {input_key}"}
                )

            updated_job = await job_client.set_input(job_id=job_id, input_key=input_key, input_id=input_id.id)
            if updated_job is None:
                raise HTTPException(
                    status_code=400, detail={"cause": "Could not update Job's inputs: likely a race condition"}
                )
            return updated_job

    @app.put(
        "/jobs/{job_id}/inputs/{input_key}/selector",
        response_model=SelectorValue,
        responses={
            200: {"description": "Job object after updating the Selector Value for an input key"},
            400: {"description": "Job with Job Id or input keyn not found"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job whose Selector Value to update"),
        input_key: str = Path(..., description="The name of the input to set"),
        selector_value: PutSelectorValue = Body(
            ..., description="Unique string used to identify a FHIR Resource in the EMPAIA namespace format"
        ),
    ) -> SelectorValue:
        """Get Job with Job Id from DB and set Selector Value and Input key in separate table.
        Raise Exception if Job Status is not in Assembly or the input key is not set."""
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            job = await get_job(job_client=job_client, job_id=job_id)
            if job.status != JobStatus.ASSEMBLY:
                raise HTTPException(
                    status_code=409,
                    detail={
                        "cause": f"Can only add Selector Value in state {JobStatus.ASSEMBLY}, but found {job.status}"
                    },
                )

            if input_key not in job.inputs:
                raise HTTPException(status_code=412, detail={"cause": f"Key not found in Job's inputs: {input_key}"})

            result = await job_client.update_selector_value(
                job_id=job_id, input_key=input_key, selector_value=selector_value.selector_value
            )

            if result is None:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": "Could not set selector_value and input_key in table: likely a race condition"},
                )
            return result

    @app.delete(
        "/jobs/{job_id}/inputs/{input_key}",
        response_model=Job,
        responses={
            200: {"description": "Job object with deleted input"},
            400: {"description": "Unknown input_key"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to delete an input from"),
        input_key: str = Path(..., description="The name of the input to delete"),
    ) -> Job:
        """Use this to delete already set inputs from the Job. Only works, if Job Status is `ASSEMBLY`.
        Raises Exception if job not found or input key does not exist or Status is not `ASSEMBLY`.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)

            job = await get_job(job_client=job_client, job_id=job_id)
            if job.status != JobStatus.ASSEMBLY:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Can only delete Inputs in state {JobStatus.ASSEMBLY}, but found {job.status}"},
                )

            if input_key not in job.inputs:
                raise HTTPException(status_code=400, detail={"cause": f"Key not in Job's inputs: {input_key}"})

            updated_job = await job_client.delete_input(job_id=job_id, input_key=input_key)
            if updated_job is None:
                raise HTTPException(
                    status_code=400, detail={"cause": "Could not update Job's inputs: likely a race condition"}
                )
            return updated_job

    @app.put(
        "/jobs/{job_id}/outputs/{output_key}",
        response_model=Job,
        responses={
            200: {"description": "Job object with added output"},
            400: {"description": "Unknown output or output already set"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to add an output to"),
        output_key: str = Path(..., description="The name of the output to set"),
        output_id: IdObject = Body(..., description="Wrapper for the actual MDS Id of the output"),
    ) -> Job:
        """To be called by app-service to add output-references to the Job in the database.
        Raises Exception if job not found or output key does not match.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            job = await get_job(job_client=job_client, job_id=job_id)

            if job.mode == JobMode.REPORT or (job.mode == JobMode.POSTPROCESSING and not job.containerized):
                if job.status != JobStatus.ASSEMBLY:
                    raise HTTPException(
                        status_code=400,
                        detail={
                            "cause": f"For job mode {job.mode} Outputs only allowed for status {JobStatus.ASSEMBLY}"
                        },
                    )
            elif job.status not in [JobStatus.READY, JobStatus.SCHEDULED, JobStatus.RUNNING]:
                raise HTTPException(
                    status_code=400,
                    detail={
                        "cause": f"Outputs allowed from {JobStatus.READY} - {JobStatus.RUNNING}, job in {job.status}"
                    },
                )

            if output_key in job.outputs:
                raise HTTPException(
                    status_code=400, detail={"cause": f"Key already has a value in Job's outputs: {output_key}"}
                )

            updated_job = await job_client.set_output(job_id=job_id, output_key=output_key, output_id=output_id.id)
            if updated_job is None:
                raise HTTPException(
                    status_code=400, detail={"cause": "Could not update Job's output: likely a race condition"}
                )
            return updated_job

    @app.delete(
        "/jobs/{job_id}/outputs/{output_key}",
        response_model=Job,
        responses={
            200: {"description": "Job object with deleted output"},
            400: {"description": "Unknown output_key"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job to delete an output from"),
        output_key: str = Path(..., description="The name of the output to delete"),
    ) -> Job:
        """Use this to delete already set outputs for the Job. Only works, if Job Mode is `PREPROCESSING` or `REPORT`.
        Raises Exception if job not found or output key does not exist or Status is not `ASSEMBLY`.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)

            job = await get_job(job_client=job_client, job_id=job_id)
            if job.mode not in [JobMode.REPORT, JobMode.POSTPROCESSING]:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Job mode {job.mode} does not support deletion of Outputs"},
                )

            if job.status != JobStatus.ASSEMBLY:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Can only delete Outputs in state {JobStatus.ASSEMBLY}, but found {job.status}"},
                )

            if output_key not in job.outputs:
                raise HTTPException(status_code=400, detail={"cause": f"Key not in Job's outputs: {output_key}"})

            updated_job = await job_client.delete_output(job_id=job_id, output_key=output_key)
            if updated_job is None:
                raise HTTPException(
                    status_code=400, detail={"cause": "Could not update Job's outputs: likely a race condition"}
                )
            return updated_job

    @app.put(
        "/jobs/{job_id}/progress",
        responses={
            200: {"description": "Job object after progress update"},
            400: {"description": "Job not found"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the Job whose progress to update"),
        job_progress: PutJobProgress = Body(..., description="New progress"),
    ) -> Job:
        """Get job from DB and update progress; may be called by the app-service.
        Raise Exception if job not found.
        """
        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            job = await job_client.update_progress(job_id=job_id, job_progress=job_progress)
            if not job:
                raise HTTPException(
                    status_code=400,
                    detail={"cause": f"Progress for job with id {str(job_id)} could not be updated"},
                )
            return job

    @app.put(
        "/jobs/{job_id}/input-validation-status",
        responses={
            200: {"description": "Job object after input validation status update"},
            400: {"description": "Job not found or invalid validation status transition"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the job whose input validation status to update"),
        status_update: PutJobValidationStatus = Body(
            ..., description="New input validation status and optional error message"
        ),
    ) -> Job:
        """
        Update input validation status.
        """
        if status_update.error_message is not None and status_update.validation_status != JobValidationStatus.ERROR:
            error_msg = "Can only set error message for state ERROR"
            raise HTTPException(status_code=400, detail={"cause": error_msg})

        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            return await job_client.update_input_validation_status(job_id, status_update)

    @app.put(
        "/jobs/{job_id}/output-validation-status",
        responses={
            200: {"description": "Job object after output validation status update"},
            400: {"description": "Job not found or invalid validation status transition"},
        },
    )
    async def _(
        job_id: UUID4 = Path(..., description="The Id of the job whose output validation status to update"),
        status_update: PutJobValidationStatus = Body(
            ..., description="New output validation status and optional error message"
        ),
    ) -> Job:
        """
        Update output validation status.
        """
        if status_update.error_message is not None and status_update.validation_status != JobValidationStatus.ERROR:
            error_msg = "Can only set error message for state ERROR"
            raise HTTPException(status_code=400, detail={"cause": error_msg})

        async with late_init.pool.acquire() as conn:
            job_client = await db_client(conn=conn)
            return await job_client.update_output_validation_status(job_id, status_update)

    @app.get(
        "/public-key",
        response_model=bytes,
        responses={
            200: {"description": "Public key for validating Access Tokens created for Jobs"},
        },
    )
    async def _() -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Jobs."""
        return access_token_tools.public_key

    async def _validate_state_transitions(put_job_status: PutJobStatus, job: Job):
        put_status = put_job_status.status
        if job.mode == JobMode.REPORT or (job.mode == JobMode.POSTPROCESSING and job.containerized is False):
            if job.status != JobStatus.ASSEMBLY or put_status not in END_STATUSES:
                raise HTTPException(
                    status_code=400,
                    detail={
                        "cause": f"Illegal status transition for mode {job.mode} from {job.status} to {put_status}"
                    },
                )
        elif JOB_STATUS_ORDER[put_status] == 0:
            raise HTTPException(
                status_code=400,
                detail={"cause": f"Illegal status transition from {job.status} to {put_status}"},
            )
