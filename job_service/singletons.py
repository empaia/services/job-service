import logging

from .empaia_sender_auth import AioHttpClient
from .settings import Settings

settings = Settings()

rsa_keys_directory = settings.rsa_keys_directory.rstrip("/")

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

http_client = AioHttpClient(
    logger=logger,
    timeout=settings.http_client_timeout,
    request_timeout=settings.request_timeout,
    auth_settings=None,
    chunk_size=settings.connection_chunk_size,
    connection_limit_per_host=settings.connection_limit_per_host,
)
