from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    disable_openapi: bool = False
    root_path: str = ""
    rsa_keys_directory: str = "./rsa/"
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    allow_legacy_apps: bool = True
    debug: bool = False
    db_host: str = "localhost"
    db_port: str = 5432
    db_username: str = "admin"
    db_password: str = "secret"
    db: str = "js"
    http_client_timeout: int = 300
    request_timeout: int = 300
    connection_limit_per_host: int = 100
    connection_chunk_size: int = 1024000
    enable_profiling: bool = False
    annot_url: str = None
    allow_any_state_transition: bool = False

    model_config = SettingsConfigDict(env_file=".env", env_prefix="JS_")
