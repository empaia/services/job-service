async def step_02(conn):
    sql = """
    CREATE TABLE v3_apps (
        id uuid PRIMARY KEY,
        ead jsonb NOT NULL
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE v3_jobs (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        app_id uuid NOT NULL,
        creator_id text NOT NULL,
        creator_type text NOT NULL,
        inputs jsonb NOT NULL,
        outputs jsonb NOT NULL,
        status text NOT NULL,
        created_at timestamp NOT NULL,
        started_at timestamp,
        ended_at timestamp,
        error_message text,
        mode text NOT NULL,
        containerized boolean NOT NULL,
        progress real
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE INDEX v3_jobs_app_id ON v3_jobs USING btree (app_id);
    CREATE INDEX v3_jobs_creator_id ON v3_jobs USING btree (creator_id);
    CREATE INDEX v3_jobs_status ON v3_jobs USING btree (status);
    CREATE INDEX v3_jobs_created_at ON v3_jobs (created_at DESC);
    CREATE INDEX v3_jobs_mode ON v3_jobs USING btree (mode);
    CREATE INDEX v3_jobs_creator_type ON v3_jobs USING btree (creator_type);
    """
    await conn.execute(sql)
