async def step_04(conn):
    sql = """
    CREATE UNIQUE INDEX v3_jobs_report_mode_creator_id ON v3_jobs (creator_id) WHERE mode = 'REPORT';
    """
    await conn.execute(sql)
