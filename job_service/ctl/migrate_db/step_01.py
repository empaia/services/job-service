from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    sql = """
    CREATE TABLE apps (
        id uuid PRIMARY KEY,
        ead jsonb NOT NULL
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE jobs (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        app_id uuid NOT NULL,
        creator_id text NOT NULL,
        creator_type text NOT NULL,
        inputs jsonb NOT NULL,
        outputs jsonb NOT NULL,
        status text NOT NULL,
        created_at timestamp NOT NULL,
        started_at timestamp,
        ended_at timestamp,
        error_message text
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE INDEX jobs_app_id ON jobs USING btree (app_id);
    CREATE INDEX jobs_creator_id ON jobs USING btree (creator_id);
    CREATE INDEX jobs_status ON jobs USING btree (status);
    CREATE INDEX jobs_created_at ON jobs (created_at DESC);
    """
    await conn.execute(sql)
