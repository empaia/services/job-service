async def step_06(conn):
    sql = """
    CREATE TABLE v3_selector_values (
        id uuid,
        input_key text NOT NULL,
        selector_value text NOT NULL,
        PRIMARY KEY (id, input_key)
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE INDEX v3_selector_values_id ON jobs USING btree (id);
    """
    await conn.execute(sql)
