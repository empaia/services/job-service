async def step_05(conn):
    sql = """
    ALTER TABLE v3_jobs
    ADD COLUMN input_validation_status TEXT,
    ADD COLUMN input_validation_error_message TEXT,
    ADD COLUMN output_validation_status TEXT,
    ADD COLUMN output_validation_error_message TEXT;
    """
    await conn.execute(sql)

    sql = """
    UPDATE v3_jobs SET
    input_validation_status = 'ERROR',
    input_validation_error_message = 'validation not enabled',
    output_validation_status = 'ERROR',
    output_validation_error_message = 'validation not enabled';
    """
    await conn.execute(sql)

    sql = """
    ALTER TABLE v3_jobs
    ALTER COLUMN input_validation_status SET NOT NULL,
    ALTER COLUMN output_validation_status SET NOT NULL;
    """
    await conn.execute(sql)

    sql = """
    CREATE INDEX v3_jobs_input_validation_status ON v3_jobs USING btree (input_validation_status);
    CREATE INDEX v3_jobs_output_validation_status ON v3_jobs USING btree (output_validation_status);
    """
    await conn.execute(sql)
