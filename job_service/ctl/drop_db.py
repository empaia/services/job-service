from asyncpg.exceptions import UndefinedTableError

from .commons import connect_db

TABLES = [
    "migration_steps",
    "js_migration_steps",
    "apps",
    "jobs",
]


async def run_drop_db():
    conn = await connect_db()

    for table in TABLES:
        try:
            await conn.execute(f"DROP TABLE {table};")
        except UndefinedTableError as e:
            print(e)
