"""
This modules defines the actual API of the Job Service. The main functions are:

- store and retrieve known EADs
- create a new Job given a Job Request
- get a Job by its ID
- set the status of a Job
- add inputs and outputs to a Job
"""

import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__
from .api.root import add_routes_root
from .api.v1 import add_routes_v1
from .api.v3 import add_routes_v3
from .late_init import LateInit
from .profiling import add_profiling
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

app = FastAPI(
    title="Job Service",
    version=__version__,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
)

late_init = LateInit()

add_routes_root(app, late_init)

app_v1 = FastAPI(openapi_url=openapi_url)
app_v3 = FastAPI(openapi_url=openapi_url)

if settings.cors_allow_origins:
    for app_obj in [app, app_v1, app_v3]:
        app_obj.add_middleware(
            CORSMiddleware,
            allow_origins=settings.cors_allow_origins,
            allow_credentials=settings.cors_allow_credentials,
            allow_methods=["*"],
            allow_headers=["*"],
        )

add_routes_v1(app_v1, late_init)
add_routes_v3(app_v3, late_init)
app.mount("/v1", app_v1)
app.mount("/v3", app_v3)

if settings.enable_profiling:
    add_profiling(app)


@app.on_event("startup")
async def startup_event():
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
