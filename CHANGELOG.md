## 0.8.8

* Extended job routes to set and get selector_value of a Job (see emp-0105)

## 0.8.7

* Updated dependencies and base images

## 0.8.6

* updated RSA keys dir for docker compose

## 0.8.5

* Updated dependencies and base images

## 0.8.4

* Updated dependencies and base images

# Changelog

## 0.8.3

* added ENV in settings:
  * allow_any_state_transition: bool = False

## 0.8.2

* Updated dependencies and base images

## 0.8.1

* Updated dependencies and base images

## 0.8.0

* migrate to pydantic v2
* updated status change to aggregate job data

## 0.7.2

* separate key-pairs per api version

## 0.7.1

* upodated models repo and refactored exception text

## 0.7.0

* added input/output validation status routes and query

## 0.6.1

* added report job mode and adapted contraints and tests

## 0.6.0

* remove EAD related functionality for v3 API

## 0.5.0

* introduce v3 API with support for preprocessing

## 0.4.50

* updated base image

## 0.4.49

* added support for profiling

## 0.4.48

* updated base image, fastapi, cryptography and postgres version

## 0.4.47

* updated base image, fastapi and postgres version

## 0.4.46

* updated base image and postgres version

## 0.4.45

* updated base image and postgres version

## 0.4.44

* updated base image version

## 0.4.43

* updated dependencies

## 0.4.42

* updated dependencies

## 0.4.41

* updated dependencies

## 0.4.40

* updated dependencies

## 0.4.39

* updated dependencies

## 0.4.38

* updated ci

## 0.4.37

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

## 0.4.36

* allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly
use an authorization header)

## 0.4.35

* updated models submodule for changes of AccessTokenTools class, which creates the RSA keys no more implicitly in the
constructor.

## 0.4.34

* create rsa keys for job token creation before starting the fastapi app, because otherwise there will be a race
condition if parallel fastapi worker processes are used and the worker processes might use different rsa keys.

## 0.4.33

* allow setting outputs from READY until RUNNING (inclusive)

## 0.4.32

* allow setting outputs only when in state RUNNING

## 0.4.31

* deprecate terminal states other than COMPLETED and ERROR

## 0.4.30

* integrate model update for JobCreatorType allowed to be "SCOPE"

## 0.4.29

* added ID validation

## 0.4.28

* adpated to refactored models.access_token_tools

## 0.4.27

* integrated shared AccessToken model and AccessTokenTools

## 0.4.26

* removed timestamps from loggin

## 0.4.25

* added new endpoint to delete job input

## 0.4.24

* setting a msg with status-change is now also possible on ERROR and TIMEOUT (and FAILED)

## 0.4.23

* calculate runtime for all end states of job

## 0.4.22

* added `runtime` to Job
* added timestamps to logger

## 0.4.21

* added `jobs` (Ids List) to JobsQuery

## 0.4.20

* catch UniqueViolationError when creating PSQL extensions, due to race conditions when sharing a DB

## 0.4.19

* added prefix for migration_steps table

## 0.4.18

* updated db-migration

## 0.4.17

* mitigate potential race condition by catching UniqueViolationError when inserting EAD

## 0.4.16

* fixed bug, where setting outputs was only possible during status ASSEMBLY

## 0.4.15

* fixed internal server error when query does not match any items

## 0.4.14

* conditional PSQL updates/deletes to avoid race conditions
* fixed bug where status ENUM values have not been inserted correctly

## 0.4.13

* replaced MongoDB with PSQL to match other micro-services in MDS
  * allows for consistent DB backups

## 0.4.12

* changed test structure

## 0.4.11

* added current version number to result of `GET /alive` route
* fixed mismatched default in get-token route

## 0.4.10

* added new route `GET /jobs{id}/token?expire=int?` to get new token at any time
* changed `POST /jobs` route to create full Job object (without access token)

## 0.4.9

* added route to delete Jobs while still in assembly

## 0.4.8

* sort jobs returned by query by `created_at`, newest first

## 0.4.7

* basic validation of EAD against JSON schema

## 0.4.6

* added Codechecks to CI and ensured compliance

## 0.4.5

* added PUT route for querying jobs with specific statuses and other attributes

## 0.4.4

* additional API documentation

## 0.4.3

* remove `_id` field when returning EAD

## 0.4.2

* added ead normalization for mongodb
  * fixing bug where `$schema` was not accepted as key

## 0.4.1

* switched to new models repo

## 0.4.0

* updated Job Model to separate EAD from Job
* added routes for posting and getting EADs
* added routes for setting inputs after Job creation

## 0.3.5

* updated Job Model (added creator_type Enum and attribute)

## 0.3.4

* updated Job Model (removed default, optional from Parameter)

## 0.3.3

* check whether all outputs are set on `COMPLETED` Jobs

## 0.3.2

* return jobs in descending order by `changed_at` field

## 0.3.1

* added `/alive` endpoint

## 0.2.1

* added ci versioning
* removed ci trigger for medical data service
