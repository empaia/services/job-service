import json
import time
import uuid

import jwt
import pytest
import requests

from job_service.models.v3.job import (
    Job,
    JobCreatorType,
    JobList,
    JobQuery,
    JobStatus,
    JobToken,
    JobValidationStatus,
    PutJobStatus,
    SelectorValue,
)

from ..singletons import js_url

# UNIT TESTS: JOB CREATION


def test_create_job():
    """create a basic job and see if anything comes back"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    assert response.status_code == 200


def test_create_job_missing_attributes():
    """try to create a job with missing required attributes"""
    two_attr_missing = {"creator_id": "creator"}
    response = requests.post(f"{js_url}/v3/jobs", json=two_attr_missing, timeout=60)
    assert response.status_code == 422
    assert "Field required" in get_error_api(response)
    assert len(response.json()["detail"]) == 2


def test_create_job_unknown_attributes():
    """try to create a job with additional unknown attributes"""
    job_req = {**basic_job_request(), "unknown": "foo"}
    response = requests.post(f"{js_url}/v3/jobs", json=job_req, timeout=60)
    assert response.status_code == 422
    assert "Extra inputs are not permitted" in get_error_api(response)


def test_create_job_illegal_attribute_value():
    """try to create a job with illegal attributes values"""
    illegal_values = {"app_id": "app", "creator_id": "creator", "creator_type": "UNKNOWN"}
    response = requests.post(f"{js_url}/v3/jobs", json=illegal_values, timeout=60)
    assert response.status_code == 422
    assert "Input should be a valid UUID" in get_error_api(response)
    assert len(response.json()["detail"]) == 2


def test_create_job_with_scope_creator_type():
    """try to create a job with legal values and "SCOPE" creator_type"""
    creator_id = str(uuid.uuid4())
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_id=creator_id, creator_type="SCOPE"), timeout=60
    )
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.creator_type == "SCOPE"


def test_create_job_with_service_creator_type():
    """try to create a job with legal values and "SERVICE" creator_type"""
    creator_id = str(uuid.uuid4())
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_id=creator_id, creator_type="SERVICE"), timeout=60
    )
    assert response.status_code == 422  # default job is a STANDALONE job which cant have a creator_type SERVICE
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_id=creator_id, creator_type="SERVICE", mode="PREPROCESSING"),
        timeout=60,
    )
    job = Job(**response.json())
    assert job.creator_type == "SERVICE"
    assert job.mode == "PREPROCESSING"


def test_create_job_check_time():
    """create a job an check that the created_at time is (close to) now"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job = Job(**response.json())
    assert check_time(job.created_at)


def test_create_preprocessing_job():
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="PREPROCESSING", creator_type="SERVICE"), timeout=60
    )
    job = Job(**response.json())
    assert job.mode == "PREPROCESSING"


def test_create_postprocessing_job():
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="POSTPROCESSING", creator_type="SCOPE"), timeout=60
    )
    job = Job(**response.json())
    assert job.mode == "POSTPROCESSING"


def test_create_uncontainerized_postprocessing_job():
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="POSTPROCESSING", creator_type="SCOPE", containerized=False),
        timeout=60,
    )
    job = Job(**response.json())
    assert job.mode == "POSTPROCESSING"
    assert not job.containerized


def test_create_uncontainerized_nonpostprocessing_job():
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="STANDALONE", containerized=False), timeout=60
    )
    assert response.status_code == 422
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="PREPROCESSING", creator_type="SERVICE", containerized=False),
        timeout=60,
    )
    assert response.status_code == 422


def test_create_report_job():
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="REPORT", creator_id=str(uuid.uuid4()), creator_type="SCOPE"),
        timeout=60,
    )
    job = Job(**response.json())
    assert job.mode == "REPORT"


def test_create_report_job_twice():
    creator_id = str(uuid.uuid4())
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="REPORT", creator_id=creator_id, creator_type="SCOPE"),
        timeout=60,
    )
    job = Job(**response.json())
    assert job.mode == "REPORT"

    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="REPORT", creator_id=creator_id, creator_type="SCOPE"),
        timeout=60,
    )
    assert response.status_code == 409


def test_create_report_job_as_service():
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="REPORT", creator_id=str(uuid.uuid4()), creator_type="SERVICE"),
        timeout=60,
    )
    assert response.status_code == 422


def test_create_get_compare_attributes_post_get():
    """create job, get same job, compare ID"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job1 = Job(**response.json())
    response = requests.get(f"{js_url}/v3/jobs/{job1.id}", timeout=60)
    assert response.status_code == 200
    job2 = Job(**response.json())
    assert job1 == job2


# UNIT TESTS: ACCESS TOKENS


def test_create_job_check_token():
    """create a job and check the Access Token"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job = Job(**response.json())
    response = requests.get(f"{js_url}/v3/public-key", timeout=60)
    public_key = response.json()
    response = requests.get(f"{js_url}/v3/jobs/{str(job.id)}/token", timeout=60)
    job_token = JobToken(**response.json())
    assert job_token.access_token is not None
    payload = jwt.decode(job_token.access_token, public_key, algorithms="RS256")
    assert payload["sub"] == str(job_token.job_id)


def test_create_job_check_token_expire():
    """create a job and check the Access Token"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job = Job(**response.json())
    response = requests.get(f"{js_url}/v3/public-key", timeout=60)
    public_key = response.json()
    start = time.time()
    # default time
    response = requests.get(f"{js_url}/v3/jobs/{job.id}/token", timeout=60)
    job_token = JobToken(**response.json())
    payload = jwt.decode(job_token.access_token, public_key, algorithms="RS256")
    time_1 = 24 * 60 * 60 - 60
    time_2 = 24 * 60 * 60
    expire = payload["exp"] - start
    print(time_1, expire, time_2)
    assert time_1 < expire <= time_2
    # custom time
    exp = 8 * 60 * 60
    response = requests.get(f"{js_url}/v3/jobs/{job.id}/token?expire={exp}", timeout=60)
    job_token = JobToken(**response.json())
    payload = jwt.decode(job_token.access_token, public_key, algorithms="RS256")
    assert exp - 60 < payload["exp"] - int(start) <= exp + 60
    # negative time
    response = requests.get(f"{js_url}/v3/jobs/{job.id}/token?expire={-12345}", timeout=60)
    assert response.status_code == 422


def test_create_job_check_token_public_key():
    """create job and check the Access Token using the public-key route"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job = Job(**response.json())
    response = requests.get(f"{js_url}/v3/jobs/{job.id}/token", timeout=60)
    job_token = JobToken(**response.json())
    response = requests.get(f"{js_url}/v3/public-key", timeout=60)
    assert response.status_code == 200
    payload = jwt.decode(job_token.access_token, response.json(), algorithms="RS256")
    assert payload["sub"] == str(job_token.job_id)


# UNIT TESTS: DELETING JOBS


def test_delete_job():
    """Create Job, then delete it, check that it's gone"""
    # setup
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    response = requests.get(f"{js_url}/v3/jobs", timeout=60)
    count = JobList(**response.json()).item_count
    # delete job
    response = requests.delete(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.id == job_id
    # check that job is deleted (and nothing else)
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 400
    response = requests.get(f"{js_url}/v3/jobs", timeout=60)
    assert JobList(**response.json()).item_count == count - 1
    response = requests.delete(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 400
    assert "not found" in get_error(response)


def test_delete_job_wrong_state():
    """Create job, update state to READY, try to delete it"""
    # setup
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=JobStatus.READY).model_dump())
    # try to delete job
    response = requests.delete(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 400
    assert "wrong state" in get_error(response)
    # job is still there
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 200


# UNIT TESTS: GETTING JOBS


def test_get_invalid_id():
    """try to get a Job with an invalid ID; this should not cause an internal server error!"""
    response = requests.get(f"{js_url}/v3/jobs/{'x'*5}", timeout=60)
    assert response.status_code == 422


def test_get_unknown_id():
    """try to get a non-existing status"""
    response = requests.get(f"{js_url}/v3/jobs/{str(uuid.uuid4())}", timeout=60)
    assert response.status_code == 400
    assert "Job not found" in get_error(response)


def test_get_standalone_job():
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SCOPE", mode="STANDALONE"), timeout=60
    )
    job_id = Job(**response.json()).id
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.mode == "STANDALONE"
    assert job.containerized
    assert job.creator_type == "SCOPE"


def test_get_preprocessing_job():
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SERVICE", mode="PREPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.mode == "PREPROCESSING"
    assert job.containerized
    assert job.creator_type == "SERVICE"


def test_get_postprocessing_job():
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SERVICE", mode="PREPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.mode == "PREPROCESSING"
    assert job.containerized
    assert job.creator_type == "SERVICE"


def test_create_get_compare_attribute():
    """create job, get same job, compare attribute"""
    request = basic_job_request()
    response = requests.post(f"{js_url}/v3/jobs", json=request, timeout=60)
    job_id = Job(**response.json()).id
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 200
    job = response.json()
    for key, _ in request.items():
        assert job[key] == request[key]


def test_get_many_jobs():
    """Get some, but not all Jobs"""
    # sleep in between creating jobs so created_at (1 second resolution) is not the same for all
    response = requests.get(f"{js_url}/v3/jobs", timeout=60)
    assert response.status_code == 200
    num_old = response.json()["item_count"]

    num_new = 10
    jobs = [requests.post(f"{js_url}/v3/jobs", json=basic_job_request()).json() for _ in range(num_new)]
    jobs = [Job.model_validate(job) for job in reversed(jobs)]
    # without skip & limit
    response = requests.get(f"{js_url}/v3/jobs", timeout=60)
    assert response.status_code == 200
    res = JobList(**response.json())
    assert res.item_count == num_old + num_new
    assert res.items[:10] == jobs
    # with skip & limit
    skip, limit = (2, 5)
    response = requests.get(f"{js_url}/v3/jobs?skip={skip}&limit={limit}", timeout=60)
    assert response.status_code == 200
    res = JobList(**response.json())
    assert res.items == jobs[skip : skip + limit]
    # assert [job["id"] for job in res.items] == [job["id"] for job in jobs[5:15]]
    # special case: limit = 0
    response = requests.get(f"{js_url}/v3/jobs?limit=0", timeout=60)
    res = JobList(**response.json())
    assert res.items == []


def test_query_jobs():
    # add a few example jobs to the DB with different app-ids & creator-ids
    app_id_1 = str(uuid.uuid4())
    app_id_2 = str(uuid.uuid4())
    creators = [str(uuid.uuid4()) for _ in range(2)]
    jobs = [
        requests.post(f"{js_url}/v3/jobs", json=basic_job_request(app_id=a, creator_id=c), timeout=60).json()
        for a in (app_id_1, app_id_2)
        for c in creators
    ]
    jobs = [Job.model_validate(job) for job in reversed(jobs)]

    # update status on some of those jobs
    updated_jobs = []
    status_updates = [None, JobStatus.READY, JobStatus.RUNNING, JobStatus.ERROR]
    for job, status in zip(jobs, status_updates):
        if status is None:
            updated_jobs.append(job)
            continue

        job_status = PutJobStatus(status=status)
        r = requests.put(f"{js_url}/v3/jobs/{job.id}/status", json=job_status.model_dump())
        r.raise_for_status()
        updated_job = Job.model_validate(r.json())

        assert updated_job.status == status
        if updated_job.status == JobStatus.ERROR:
            assert updated_job.ended_at is not None
        else:
            assert updated_job.ended_at is None
        if updated_job.status == JobStatus.RUNNING:
            assert updated_job.started_at is not None
            assert updated_job.runtime is not None

        updated_jobs.append(updated_job)
    # Jobs: [(app_id_1, creator1, ASSEMBLY), (app_id_1, creator2, READY),
    #        (app_id_2, creator1, RUNNING), (app_id_2, creator2, ERROR)]

    def filter_jobs(jobs, job_query, skip=None, limit=None):
        filtered = []
        for job in jobs:
            if job_query.creators is not None and job.creator_id not in job_query.creators:
                continue
            if job_query.jobs is not None and job.id not in job_query.jobs:
                continue
            if job_query.apps is not None and job.app_id not in job_query.apps:
                continue
            if job_query.statuses is not None and job.status not in job_query.statuses:
                continue
            filtered.append(job)
        if skip is not None:
            filtered = filtered[skip:]
        if limit is not None:
            filtered = filtered[:limit]
        return filtered

    def paging_params(skip=None, limit=None):
        paging = ""
        if skip is not None:
            paging = f"?skip={skip}"
        if limit is not None:
            if paging:
                paging += f"&limit={limit}"
            else:
                paging = f"?limit={limit}"
        return paging

    def query(jobs, job_query, skip=None, limit=None):
        filtered_jobs = filter_jobs(jobs=jobs, job_query=job_query, skip=skip, limit=limit)
        q = f"{js_url}/v3/jobs/query{paging_params(skip=skip, limit=limit)}"
        r = requests.put(q, json=json.loads(job_query.model_dump_json()))
        assert r.status_code == 200
        response_jobs = JobList(**r.json())
        response_ids = [j.id for j in list(response_jobs.items)]
        filtered_ids = [f.id for f in filtered_jobs]
        assert len(response_ids) == len(filtered_ids)
        assert response_ids == filtered_ids

    # paging
    query(jobs=updated_jobs, job_query=JobQuery(creators=creators))
    skip, limit = 1, 2
    query(jobs=updated_jobs, job_query=JobQuery(creators=creators), skip=skip, limit=limit)
    skip, limit = 5, 0
    query(jobs=updated_jobs, job_query=JobQuery(creators=creators), skip=skip, limit=limit)
    # matching
    query(jobs=updated_jobs, job_query=JobQuery(jobs=[str(updated_jobs[0].id)]))
    query(jobs=updated_jobs, job_query=JobQuery(creators=creators, apps=[app_id_1, app_id_2]))
    query(jobs=updated_jobs, job_query=JobQuery(creators=creators[:1], apps=[app_id_2]))
    query(jobs=updated_jobs, job_query=JobQuery(creators=creators, statuses=["READY", "RUNNING", "ERROR"]))
    query(jobs=updated_jobs, job_query=JobQuery(creators=creators[:1], statuses=["ERROR"]))


def test_query_by_validation_state():
    jobs = []
    for _ in range(3):
        job = {}
        response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
        assert response.status_code == 200
        jobs.append(response.json())

    query = {"jobs": [job["id"] for job in jobs], "input_validation_statuses": ["RUNNING"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 0

    query = {"jobs": [job["id"] for job in jobs], "input_validation_statuses": ["NONE"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 3

    query = {"jobs": [job["id"] for job in jobs], "output_validation_statuses": ["RUNNING"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 0

    query = {"jobs": [job["id"] for job in jobs], "output_validation_statuses": ["NONE"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 3

    job_id = jobs[0]["id"]
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/input-validation-status", json={"validation_status": "RUNNING"}, timeout=60
    )
    assert response.status_code == 200
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/output-validation-status", json={"validation_status": "COMPLETED"}, timeout=60
    )
    assert response.status_code == 200

    query = {"jobs": [job["id"] for job in jobs], "input_validation_statuses": ["NONE"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 2

    query = {"jobs": [job["id"] for job in jobs], "output_validation_statuses": ["NONE"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 2

    query = {"jobs": [job["id"] for job in jobs], "input_validation_statuses": ["NONE", "RUNNING"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 3

    query = {"jobs": [job["id"] for job in jobs], "output_validation_statuses": ["NONE", "COMPLETED"]}
    response = requests.put(f"{js_url}/v3/jobs/query", json=query, timeout=60)
    assert response.status_code == 200
    job_list = response.json()
    assert job_list["item_count"] == 3


def test_query_jobs_by_modes():
    jobs = []
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="PREPROCESSING", creator_type="SERVICE"), timeout=60
    )
    jobs.append(Job(**response.json()))
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="POSTPROCESSING", creator_type="SCOPE"), timeout=60
    )
    jobs.append(Job(**response.json()))
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="STANDALONE", creator_type="SCOPE"), timeout=60
    )
    jobs.append(Job(**response.json()))
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="REPORT", creator_id=str(uuid.uuid4()), creator_type="SCOPE"),
        timeout=60,
    )
    jobs.append(Job(**response.json()))
    job_ids = list(map(lambda job: str(job.id), jobs))

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 4

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "modes": ["PREPROCESSING"]}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 1
    assert job_list.items[0].mode == "PREPROCESSING"

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "modes": ["POSTPROCESSING"]}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 1
    assert job_list.items[0].mode == "POSTPROCESSING"

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "modes": ["STANDALONE"]}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 1
    assert job_list.items[0].mode == "STANDALONE"

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "modes": ["REPORT"]}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 1
    assert job_list.items[0].mode == "REPORT"

    response = requests.put(
        f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "modes": ["PREPROCESSING", "POSTPROCESSING"]}, timeout=60
    )
    job_list = JobList(**response.json())
    assert len(job_list.items) == 2
    assert job_list.items[0].mode == "PREPROCESSING" or job_list.items[1].mode == "PREPROCESSING"
    assert job_list.items[0].mode == "POSTPROCESSING" or job_list.items[1].mode == "POSTPROCESSING"


def test_query_jobs_by_creator_types():
    jobs = []
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="PREPROCESSING", creator_type="SERVICE"), timeout=60
    )
    jobs.append(Job(**response.json()))
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="POSTPROCESSING", creator_type="SCOPE"), timeout=60
    )
    jobs.append(Job(**response.json()))
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="STANDALONE", creator_type="SCOPE"), timeout=60
    )
    jobs.append(Job(**response.json()))
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="STANDALONE", creator_type="USER"), timeout=60
    )
    jobs.append(Job(**response.json()))
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(mode="REPORT", creator_id=str(uuid.uuid4()), creator_type="SCOPE"),
        timeout=60,
    )
    jobs.append(Job(**response.json()))
    job_ids = list(map(lambda job: str(job.id), jobs))

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 5

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "creator_types": ["SERVICE"]}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 1
    assert job_list.items[0].creator_type == "SERVICE"

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "creator_types": ["SCOPE"]}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 3
    assert job_list.items[0].creator_type == "SCOPE"

    response = requests.put(f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "creator_types": ["USER"]}, timeout=60)
    job_list = JobList(**response.json())
    assert len(job_list.items) == 1
    assert job_list.items[0].creator_type == "USER"

    response = requests.put(
        f"{js_url}/v3/jobs/query", json={"jobs": job_ids, "creator_types": ["SCOPE", "SERVICE"]}, timeout=60
    )
    job_list = JobList(**response.json())
    assert len(job_list.items) == 4


def test_query_jobs_no_match():
    job_query = JobQuery(creators=["unknown"])
    q = f"{js_url}/v3/jobs/query"
    r = requests.put(q, json=job_query.model_dump())
    assert r.status_code == 200
    data = r.json()
    assert data["items"] == []
    assert data["item_count"] == 0


# UNIT TESTS: JOB STATUS


def test_set_status():
    """create job and test different legal and illegal status transitions, get job and compare status"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    status_updates = [
        ("SCHEDULED", True),
        ("RUNNING", True),
        ("ERROR", True),
        ("COMPLETED", False),
    ]
    for status, okay in status_updates:
        response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=status).model_dump())
        if okay:
            assert response.status_code == 200
            job = Job(**response.json())
            assert job.status == status
        else:
            assert response.status_code == 400
            assert "Illegal status transition" in get_error(response)
    # check that new status is that of the last valid update
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.status == "ERROR"


def test_set_status_deprecated():
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    deprecated = ["FAILED", "INCOMPLETE", "TIMEOUT"]
    for status in deprecated:
        response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=status).model_dump())
        assert response.status_code == 400
        assert "deprecated" in get_error(response)


def test_set_status_invalid_id():
    """try to set the status of a job with an illegal id"""
    response = requests.get(f"{js_url}/v3/jobs/{'x'*5}", timeout=60)
    assert response.status_code == 422


def test_set_status_unknown_id():
    """try to set the status of a job that does not exist"""
    response = requests.get(f"{js_url}/v3/jobs/{uuid.uuid4()}", timeout=60)
    assert response.status_code == 400
    assert "does not exist" in get_error(response)


def test_set_status_illegal():
    """try to set an illegal status or non-error status with error message"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    # unknown status
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json={"status": "WHATEVER"}, timeout=60)
    assert response.status_code == 422
    assert "Input should be 'NONE', 'ASSEMBLY'" in get_error_api(response)
    # error message on non-failed status
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/status", json={"status": "SCHEDULED", "error_message": "whops"}, timeout=60
    )
    assert response.status_code == 400
    assert "Can only set status message for" in get_error(response)


# UNIT TESTS: JOB INPUTS


def test_add_input_invalid_id():
    """create job and add an input to job with illegal id"""
    response = requests.put(f"{js_url}/v3/jobs/{'x'*5}/inputs/key", json={"id": "12345"}, timeout=60)
    assert response.status_code == 422


def test_add_input_unknown_id():
    """create job and add an input to job with unknown id"""
    response = requests.put(f"{js_url}/v3/jobs/{uuid.uuid4()}/inputs/key", json={"id": "12345"}, timeout=60)
    assert response.status_code == 400
    assert "does not exist" in get_error(response)


def test_add_input():
    """create job and add an input to the job"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.inputs["blib"] == "12345"
    # check updated value after new get-job
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.inputs["blib"] == "12345"


def test_add_selector_value_to_input():
    """create a job and store the selector_value and input_key"""
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SERVICE", mode="PREPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    # add input
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_1", json={"id": "12345|1"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.inputs["my_questionnaire_1"] == "12345|1"
    # add selector_value for input_key
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_1/selector",
        json={"selector_value": "org.empaia.global.v1.selectors"},
        timeout=60,
    )
    assert response.status_code == 200
    selector_value_result = SelectorValue(**response.json())
    assert selector_value_result.id == job.id
    assert selector_value_result.input_key == "my_questionnaire_1"
    assert selector_value_result.selector_value == "org.empaia.global.v1.selectors"


def test_add_input_interactive_job():
    """create an interactive job and add an input to the job"""
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SCOPE", mode="POSTPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.inputs["blib"] == "12345"
    # check updated value after new get-job
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.inputs["blib"] == "12345"


def test_add_input_report_job():
    """create a report job and add an input to the job"""
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", creator_id=str(uuid.uuid4()), mode="REPORT"),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 400


def test_delete_unset_input():
    """create job, add and delete an unset input from the job"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    # delete unset input
    response = requests.delete(f"{js_url}/v3/jobs/{job_id}/inputs/blib", timeout=60)
    assert response.status_code == 400
    assert response.json()["detail"]["cause"].startswith("Key not in Job's inputs")


def test_delete_input():
    """create job, add and delte an input"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.inputs["blib"] == "12345"
    # check delete input
    response = requests.delete(f"{js_url}/v3/jobs/{job_id}/inputs/blib", timeout=60)
    job = Job(**response.json())
    assert "blib" not in job.inputs


def test_add_input_overwrite():
    """try to set an input again that has already been set before"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 400
    assert "already has a value" in get_error(response)
    # actual value is the value after the first update, not the second
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.inputs["blib"] == "12345"


@pytest.mark.skip
def test_add_input_id_does_not_exist():
    """check whether the input-id exists; not sure if we want to check"""
    # currently not implemented, not sure if we really want this
    # if we do, also have to adapt the other input-tests (mock medical data service?)
    assert False


def test_add_selector_value_input_key_does_not_exist():
    """create a job and add an input and a selector_value to the job but input key does not exist"""
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SERVICE", mode="PREPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    # add input
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_3", json={"id": "12345|1"}, timeout=60)
    assert response.status_code == 200
    # add selector_value
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/inputs/non_existing/selector",
        json={"selector_value": "org.empaia.global.v1.selectors"},
        timeout=60,
    )
    assert response.status_code == 412
    assert "Key not found in Job's inputs" in get_error(response)


def test_get_selector_value():
    """get a selector_value by job_id and input_key"""
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SERVICE", mode="PREPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    # add input
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_4", json={"id": "12345|1"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    # add selector_value
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_4/selector",
        json={"selector_value": "org.empaia.global.v1.selectors"},
        timeout=60,
    )
    assert response.status_code == 200
    selector_value_result = SelectorValue(**response.json())
    assert selector_value_result.id == job.id
    assert selector_value_result.input_key == "my_questionnaire_4"
    assert selector_value_result.selector_value == "org.empaia.global.v1.selectors"
    # get selector_value
    response = requests.get(f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_4/selector", timeout=60)
    assert response.status_code == 200
    selector_value = SelectorValue(**response.json())
    assert selector_value.selector_value == "org.empaia.global.v1.selectors"


def test_get_selector_value_does_not_exist():
    """get a selector_value by job_id with non-existent input_key"""
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SERVICE", mode="PREPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    # add input
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_5", json={"id": "12345|1"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    # add selector_value
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_5/selector",
        json={"selector_value": "org.empaia.global.v1.selectors"},
        timeout=60,
    )
    assert response.status_code == 200
    selector_value_result = SelectorValue(**response.json())
    assert selector_value_result.id == job.id
    assert selector_value_result.input_key == "my_questionnaire_5"
    assert selector_value_result.selector_value == "org.empaia.global.v1.selectors"
    # get selector_value
    response = requests.get(f"{js_url}/v3/jobs/{job_id}/inputs/non_existing/selector", timeout=60)
    assert response.status_code == 412


def test_modify_selector_value_after_setting():
    """create a job and modify the selector_value after it has been set"""
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(creator_type="SERVICE", mode="PREPROCESSING"), timeout=60
    )
    job_id = Job(**response.json()).id
    # add input
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_6", json={"id": "12345|1"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.inputs["my_questionnaire_6"] == "12345|1"
    # add selector_value for input_key
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_6/selector",
        json={"selector_value": "org.empaia.global.v1.selectors"},
        timeout=60,
    )
    assert response.status_code == 200
    selector_value_result = SelectorValue(**response.json())
    assert selector_value_result.id == job.id
    assert selector_value_result.input_key == "my_questionnaire_6"
    assert selector_value_result.selector_value == "org.empaia.global.v1.selectors"
    # modify selector_value for input_key and job_id
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/inputs/my_questionnaire_6/selector",
        json={"selector_value": "org.empaia.global.v1.selectors_2"},
        timeout=60,
    )
    assert response.status_code == 200
    selector_value_result = SelectorValue(**response.json())
    assert selector_value_result.id == job.id
    assert selector_value_result.input_key == "my_questionnaire_6"
    assert selector_value_result.selector_value == "org.empaia.global.v1.selectors_2"


def test_add_input_wrong_status():
    """try to add an input to the job when the job is not running"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status="READY").model_dump())
    assert response.status_code == 200
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 400
    assert "only add Inputs in state JobStatus.ASSEMBLY" in get_error(response)


def test_delete_input_wrong_status():
    """try to delete an input from the job when the job is not ASSEMBLY"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.inputs["blib"] == "12345"
    # set to READY
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status="READY").model_dump())
    assert response.status_code == 200
    # check delete input when READY
    response = requests.delete(f"{js_url}/v3/jobs/{job_id}/inputs/blib", timeout=60)
    assert response.status_code == 400
    assert response.json()["detail"]["cause"].startswith("Can only delete Inputs in state")


# UNIT TESTS: JOB OUTPUTS


def test_add_output_invalid_id():
    """create job and add an output to job with illegal id"""
    response = requests.put(f"{js_url}/v3/jobs/{'x'*5}/outputs/key", json={"id": "12345"}, timeout=60)
    assert response.status_code == 422


def test_add_output_unknown_id():
    """create job and add an output to job with unknown id"""
    response = requests.put(f"{js_url}/v3/jobs/{uuid.uuid4()}/outputs/key", json={"id": "12345"}, timeout=60)
    assert response.status_code == 400
    assert "does not exist" in get_error(response)


def test_add_output():
    """create job and add an output to the job"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    requests.put(f"{js_url}/v3/jobs/{job_id}/status", json={"status": "RUNNING"}, timeout=60)
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.outputs["blub"] == "12345"
    # check updated value after new get-job
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.outputs["blub"] == "12345"


def test_add_output_report():
    """create report job and add an output to the job in state assembly"""
    creator_id = str(uuid.uuid4())
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", creator_id=creator_id, mode="REPORT"),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200


def test_add_output_postproocessing_non_containerized():
    """create non-containerized postprocessing job and add an output to the job in state assembly"""
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", mode="POSTPROCESSING", containerized=False),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200


def test_add_output_postproocessing_containerized():
    """create report job and add an output to the job in state assembly"""
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", mode="POSTPROCESSING", containerized=True),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 400
    requests.put(f"{js_url}/v3/jobs/{job_id}/status", json={"status": "RUNNING"}, timeout=60)
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200


def test_add_output_overwrite():
    """try to set an output again that has already been set before"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    requests.put(f"{js_url}/v3/jobs/{job_id}/status", json={"status": "RUNNING"}, timeout=60)
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "abcde"}, timeout=60)
    assert response.status_code == 400
    assert "already has a value" in get_error(response)
    # actual value is the value after the first update, not the second
    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    job = Job(**response.json())
    assert job.outputs["blub"] == "12345"


@pytest.mark.skip
def test_add_output_id_does_not_exist():
    """check whether the output-id exists; not sure if we want to check"""
    # currently not implemented, not sure if we really want this
    # if we do, also have to adapt the other output-tests (mock medical data service?)
    assert False


def test_add_output_wrong_status():
    """try to add an output to the job when the job is not running"""
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 400


# UNIT TESTS: JOB PROGRESS


def test_update_job_progress():
    unknown_id = str(uuid.uuid4())
    response = requests.put(f"{js_url}/v3/jobs/{unknown_id}/progress", json={"progress": 0.25}, timeout=60)
    assert response.status_code == 400
    response = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    job = Job(**response.json())
    assert job.progress is None
    response = requests.put(f"{js_url}/v3/jobs/{job.id}/progress", json={"progress": -1.0}, timeout=60)
    assert response.status_code == 422
    response = requests.put(f"{js_url}/v3/jobs/{job.id}/progress", json={"progress": 1.2}, timeout=60)
    assert response.status_code == 422
    response = requests.put(f"{js_url}/v3/jobs/{job.id}/progress", json={"progress": 0.5}, timeout=60)
    job = Job(**response.json())
    assert job.progress == pytest.approx(0.5)
    response = requests.get(f"{js_url}/v3/jobs/{job.id}", timeout=60)
    job = Job(**response.json())
    assert job.progress == pytest.approx(0.5)


# UNIT TESTS: JOB RUNTIME


def test_job_runtime_valid_run():
    r = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    assert r.status_code == 200
    job = Job(**r.json())
    # status: ASSEMBLY
    assert job.status == JobStatus.ASSEMBLY
    assert job.runtime is None

    # add input
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert r.status_code == 200

    # set status: READY
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/status", json=PutJobStatus(status=JobStatus.READY).model_dump())
    assert r.status_code == 200
    job = Job(**r.json())
    # status: READY
    assert job.status == JobStatus.READY
    assert job.runtime is None

    # set status: SCHEDULED
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/status", json=PutJobStatus(status=JobStatus.SCHEDULED).model_dump())
    assert r.status_code == 200
    job = Job(**r.json())
    # status: SCHEDULED
    assert job.status == JobStatus.SCHEDULED
    assert job.runtime is None

    # set status: RUNNING
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/status", json=PutJobStatus(status=JobStatus.RUNNING).model_dump())
    assert r.status_code == 200
    job = Job(**r.json())
    # status: RUNNING
    assert job.status == JobStatus.RUNNING
    assert job.runtime >= 0

    # add output
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert r.status_code == 200

    # set status: COMPLETED
    time.sleep(2)
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/status", json=PutJobStatus(status=JobStatus.COMPLETED).model_dump())
    assert r.status_code == 200
    job = Job(**r.json())
    # status: COMPLETED
    assert job.status == JobStatus.COMPLETED
    assert job.runtime >= 2


def test_job_runtime_error():
    r = requests.post(f"{js_url}/v3/jobs", json=basic_job_request(), timeout=60)
    assert r.status_code == 200
    job = Job(**r.json())
    # status: ASSEMBLY
    assert job.status == JobStatus.ASSEMBLY
    assert job.runtime is None

    # add input
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/inputs/blib", json={"id": "12345"}, timeout=60)
    assert r.status_code == 200

    # set status: READY
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/status", json=PutJobStatus(status=JobStatus.READY).model_dump())
    assert r.status_code == 200
    job = Job(**r.json())
    # status: READY
    assert job.status == JobStatus.READY
    assert job.runtime is None

    # set status: ERROR
    r = requests.put(f"{js_url}/v3/jobs/{job.id}/status", json=PutJobStatus(status=JobStatus.ERROR).model_dump())
    assert r.status_code == 200
    job = Job(**r.json())
    # status: READY
    assert job.status == JobStatus.ERROR
    assert job.runtime is None


def test_report_job_state_transmissions():
    """test report job state transmissions"""
    creator_id = str(uuid.uuid4())
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", creator_id=creator_id, mode="REPORT"),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    for status in [JobStatus.READY, JobStatus.SCHEDULED, JobStatus.RUNNING]:
        response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=status).model_dump())
        print(response.content)
        assert response.status_code == 400
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=JobStatus.COMPLETED).model_dump()
    )
    assert response.status_code == 200


def test_report_job_state_runtime_error():
    """test report job runtime error"""
    creator_id = str(uuid.uuid4())
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", creator_id=creator_id, mode="REPORT"),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=JobStatus.ERROR).model_dump())
    assert response.status_code == 200


def test_non_containerized_postprocessing_job_runtime_error():
    """test non containerized postprocessing job runtime error"""
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", mode="POSTPROCESSING", containerized=False),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=JobStatus.ERROR).model_dump())
    assert response.status_code == 200


def test_non_containerized_postprocessing_job_state_transmissions():
    """test non containerized postprocessing job state transmissions"""
    response = requests.post(
        f"{js_url}/v3/jobs",
        json=basic_job_request(creator_type="SCOPE", mode="POSTPROCESSING", containerized=False),
        timeout=60,
    )
    job_id = Job(**response.json()).id
    response = requests.put(f"{js_url}/v3/jobs/{job_id}/outputs/blub", json={"id": "12345"}, timeout=60)
    assert response.status_code == 200
    for status in [JobStatus.READY, JobStatus.SCHEDULED, JobStatus.RUNNING]:
        response = requests.put(f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=status).model_dump())
        print(response.content)
        assert response.status_code == 400
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/status", json=PutJobStatus(status=JobStatus.COMPLETED).model_dump()
    )
    assert response.status_code == 200


# UNIT TESTS: JOB VALIDATION STATUS


def test_validation_status_api():
    response = requests.post(
        f"{js_url}/v3/jobs", json=basic_job_request(mode="STANDALONE", creator_type="SCOPE"), timeout=60
    )
    assert response.status_code == 200
    job = Job(**response.json())
    job_id = job.id
    assert job.input_validation_status == JobValidationStatus.NONE
    assert job.output_validation_status == JobValidationStatus.NONE
    assert not job.input_validation_error_message
    assert not job.output_validation_error_message

    validation_status_update = {"validation_status": "RUNNING", "error_message": "Should not work"}
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/input-validation-status", json=validation_status_update, timeout=60
    )
    assert response.status_code == 400

    validation_status_update = {"validation_status": "RUNNING"}
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/input-validation-status", json=validation_status_update, timeout=60
    )
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.input_validation_status == JobValidationStatus.RUNNING
    assert job.output_validation_status == JobValidationStatus.NONE

    validation_status_update = {"validation_status": "RUNNING"}
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/output-validation-status", json=validation_status_update, timeout=60
    )
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.input_validation_status == JobValidationStatus.RUNNING
    assert job.output_validation_status == JobValidationStatus.RUNNING

    validation_status_update = {"validation_status": "ERROR", "error_message": "Something went wrong"}
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/output-validation-status", json=validation_status_update, timeout=60
    )
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.input_validation_status == JobValidationStatus.RUNNING
    assert job.output_validation_status == JobValidationStatus.ERROR
    assert job.output_validation_error_message == "Something went wrong"

    validation_status_update = {"validation_status": "COMPLETED"}
    response = requests.put(
        f"{js_url}/v3/jobs/{job_id}/input-validation-status", json=validation_status_update, timeout=60
    )
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.input_validation_status == JobValidationStatus.COMPLETED
    assert job.output_validation_status == JobValidationStatus.ERROR

    response = requests.get(f"{js_url}/v3/jobs/{job_id}", timeout=60)
    assert response.status_code == 200
    job = Job(**response.json())
    assert job.input_validation_status == JobValidationStatus.COMPLETED
    assert job.output_validation_status == JobValidationStatus.ERROR
    assert job.input_validation_error_message is None
    assert job.output_validation_error_message == "Something went wrong"


# HELPER FUNCTIONS


def create_sample_ead():
    return {
        "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
        "name": "My app",
        "name_short": "my_app",
        "namespace": "org.empaia.my_vendor.my_app.v3.0",
        "description": "Human readable description",
        "configuration": {
            "global": {
                "private_api_username": {"type": "string", "optional": False},
                "private_api_password": {"type": "string", "optional": False},
            },
            "customer": {
                "private_api_username": {"type": "string", "optional": False},
                "private_api_password": {"type": "string", "optional": False},
            },
        },
        "permissions": {
            "data_transmission_to_external_service_provider": True,
        },
        "io": {
            "blib": {"type": "integer"},
            "blub": {"type": "integer"},
        },
        "modes": {
            "standalone": {
                "inputs": ["blib"],
                "outputs": ["blub"],
            }
        },
    }


def basic_job_request(
    app_id=None, creator_id="creator", creator_type=JobCreatorType.USER, mode="STANDALONE", containerized=True
):
    if app_id is None:
        app_id = str(uuid.uuid4())
    return {
        "app_id": app_id,
        "creator_id": creator_id,
        "creator_type": creator_type,
        "mode": mode,
        "containerized": containerized,
    }


def check_time(timestamp):
    return abs(timestamp - time.time()) <= 5


def get_error(response):
    return response.json()["detail"]["cause"]


def get_error_api(response):
    return response.json()["detail"][0]["msg"]
