import requests

from .singletons import js_url


def test_alive():
    response = requests.get(f"{js_url}/alive", timeout=60)
    assert response.status_code == 200
    assert response.json()["status"] == "ok"
