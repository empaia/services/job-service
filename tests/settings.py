from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    js_url: str

    model_config = SettingsConfigDict(extra="allow", env_file=".env", env_prefix="PYTEST_")
