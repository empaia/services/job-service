# job-service

Service for managing both Apps/EADs and actual Jobs.

## Build, Environment Variables & Startup

Environment Variables:

* `JS_ALLOW_LEGACY_APPS` is used to allow/disallow "legacy" Apps from being added to the Job Service's App Cache.
  A production system should allow this, so older Apps from the Solution Store can be used, but if the Job Service
  is used as part of the EMPAIA App Test Suite, it should not be allowed. Defaults to `True`
* `JS_RSA_KEYS_DIRECTORY` is the directory where to find the private and public keys used to generate  and to validate
  the Access Tokens, in the files `id_rsa` and `id_rsa.pub` respectively. If no directory is specified, `./rsa/` will
  be used. If the directory or the files do not exist, a new key pair will be generated and stored in that location.
  When run in a Docker container, it is suggested syncing this directory with a directory on the host system, so the
  key remains when the container shuts down, e.g.,
  ```
  docker run -p 8000:8000 -e RSA_KEYS_DIRECTORY=/app/rsa -v `pwd`/RSA:/app/rsa job-service
  ```
* `JS_ROOT_PATH` set if server runs behind a proxy, else openapi `/docs` will not work.
* `JS_DISABLE_OPENAPI` If you want to disable openapi `/docs` in productive mode.
* `JS_CORS_ALLOW_CREDENTIALS` to enable browser credentials (requires specific origins in JS_CORS_ALLOW_ORIGINS, does 
not work with "*")
* `JS_CORS_ALLOW_ORIGINS` to enable Cross-Origin Resource Sharing (CORS); can be a list of allowed origins, like
  `["http://origin1","https://origin2:80"]`, or wildcard `["*"]` (incl. the `[...]`) for all origins; default: `None`.

```
cd job-service
python -m venv .venv
source .venv/bin/activate
poetry install
```

Setup environment:

```bash
cp sample.env .env  # adapt as needed
```

Start service:

```bash
docker-compose up --build -d
```

### Automated Checks

Checks are enforced in CI, so check your code before committing!

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
black .
isort .
pycodestyle job_service
pylint job_service
pytest tests
```

## API Documentation

Please see OpenAPI specification hosted at `/<api-version>/docs`, e.g.,
* [API v1 Documentation](http://localhost:8000/v1/docs)
* [API v3 Documentation](http://localhost:8000/v1/docs)
