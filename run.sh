#!/usr/bin/env bash

jsctl create-access-token-tool-keys && jsctl migrate-db && uvicorn job_service.app:app $@
